next step roles:
----------------------------------------------------------------------------------------
manipulator (mafia role that wins if they make it to bottom3)

Then Seeker/~~Executioner~~/Trickster, who all require changing game elements I believe

~~Mayor~~, medical examiner, and lookout also on the list of 8s but we weren't sure about the way we wanted to do mayor last I recall

Seeker learns who their target is targeting each night. Opposite the lookout who finds out the people targeting their target


We need to ensure that primaryTarget and previousTarget are kept up to date for ANY class that is going to actually have an impact on target (i.e. Medic, Mafia, Cop, Vigilante, etc).  
Lookout will need to query EVERYONE BEFORE THEM's previousTarget and EVERYONE AFTER THEM's primaryTarget.