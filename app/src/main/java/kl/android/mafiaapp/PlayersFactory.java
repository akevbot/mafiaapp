package kl.android.mafiaapp;

import android.content.Context;

import java.util.Map;
import java.util.Random;


import kl.android.mafiaapp.player.*;

/**
 * Created by Kevin on 6/2/2017.
 */

public class PlayersFactory {

    public static Context context;

    public static Player[] createPlayerArray(int numMafia, String[] names,
                                             Map isEnabled, Context context) {
        PlayersFactory.context = context;
        // init player array
        Player[] players = new Player[names.length];
        // init rng
        Random rng = new Random();
        int randomNumber = rng.nextInt(players.length);

        // picks x amount of Mafia members, COMPLETELY RANDOMLY
        // uses RNG to pick any number, and if already initialized, rolls again
        for (int i = 0; i < numMafia; i++) {
            while (players[randomNumber] != null) {
                randomNumber = rng.nextInt(players.length);
            }
            players[randomNumber] = new Mafia(names[randomNumber]);
        }

        // other stuff goes in here
        for (Role role : Role.values()) {
            if ((Boolean)isEnabled.get(role)) {
                while (players[randomNumber] != null) {
                    randomNumber = rng.nextInt(players.length);
                }
                players[randomNumber] = createClassByName(names[randomNumber], role);
            }
        }
        // make any left overs Townspeople
        for (int i = 0; i < players.length; i++)
            if (players[i] == null)
                players[i] = new Town(names[i]);

        return players;
    }

    // Need to update this method to include any new Role classes,
    // as well as Role.java enum, and wherever logic is needed (obviously)
    public static Player createClassByName(String name, Role role)
    {
        // then switch based on the results.
        switch (role) {
            case OFFICER: return new Officer(name);
            case MEDIC: return new Medic(name);
            case VIGILANTE: return new Vigilante(name);
            case VETERAN: return new Veteran(name);
            case ELDER: return new Elder(name);
            case MARTYR: return new Martyr(name);
            //TODO: add settings to choose how many bullets Survivalist can tank
            case SURVIVALIST: return new Survivalist(name, 1);
            case MAYOR: return new Mayor(name);
            case EXECUTIONER: return new Executioner(name);
        }

        return new Town(name);
    }

}
