package kl.android.mafiaapp;

import kl.android.mafiaapp.player.Player;

/**
 * Created by kevin on 8/13/17.
 */

public class VoteManager {
    private int numberOfLivePlayers;
    private int votesToKill;
    private String accused;

    public VoteManager(Player[] players, String playerToKill) {
        numberOfLivePlayers = 0;
        for (Player player : players)
            if (!player.isDead())
                numberOfLivePlayers++;
        votesToKill = 0;
        accused = playerToKill;
    }

    public void vote(int votePower) {
            votesToKill += votePower;
    }

    public Boolean voteSuccessful() {
        // if majority, vote successful
        return (votesToKill >= numberOfLivePlayers - (numberOfLivePlayers / 2));
    }

    public String getAccused() {
        return accused;
    }
}
