package kl.android.mafiaapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import kl.android.mafiaapp.player.Mafia;
import kl.android.mafiaapp.player.Martyr;
import kl.android.mafiaapp.player.Player;
import kl.android.mafiaapp.player.Veteran;
import kl.android.mafiaapp.player.Vigilante;

/**
 * Created by Kevin on 5/25/2017.
 */

/*
 * isEnabled is a map.  You reference its variables by using a key.
 * in this case, you would do isEnabled.get("Medic"), for example, and this would return
 * true or false.  You can also use put("Medic", true), etc.
 */

public class GameManager implements Serializable {
    private Player[] players;
    private int numMafia;
    private Map isEnabled;
    private int currentPlayer = 0;
    private GameState state;
    public String results = "";
    private int unsuccessfulMotions = 0;
    private VoteManager vm;
    private Context context;

    public GameManager(String[] names, int mafia, Map enabled, Context context) {
        // grab settings values
        numMafia = mafia;
        isEnabled = enabled;

        // initialize array as same size of names array
        players = PlayersFactory.createPlayerArray(numMafia, names, isEnabled, context);
    }

    public void setContext(Context ctx) {
        context = ctx;
    }

    public boolean pastLastPlayer() {
        if (currentPlayer < players.length)
            return false;
        currentPlayer = 0;
        return true;
    }

    public String nightOverComputation() {
        results = new NightCalculation(players).nightOverComputation();
        return results;
    }

    public void resetAll() {
        for (Player p : players) {
            p.reset();
            p.resetClassTarget(this);
        }
    }

    public void setState(GameState newState) {
        state = newState;
    }

    public GameState getState() {
        return state;
    }

    public String gameOver() {
        int aliveMafia = 0, aliveTown = 0;
        for (Player player : players) {
            if (player.isDead())
                continue;
            if (player instanceof Mafia)
                aliveMafia++;
            else aliveTown++;
        }
        if (aliveMafia == 0)
            return Constants.TOWN_WIN;
        if (aliveMafia >= aliveTown)
            return Constants.MAFIA_WIN;
        if (aliveMafia + aliveTown <= 3)
            return Constants.FINAL_THREE;
        return Constants.GAME_CONTINUE;
    }

    public void advanceCurrentPlayer() {
        currentPlayer++;
    }


    public void advanceGame(String spinnerValue) {
        switch (getState()) {
            case SHOW_ROLE:
                advanceCurrentPlayer();
                if (pastLastPlayer())
                    state = GameState.NIGHT;
                break;
            case NIGHT:
                // set live players' targets
                if (!getCurrent().isDead()) {
                    getCurrent().setPrimaryTarget(getIndexByName(spinnerValue));
                    getCurrent().setClassTarget(this, getIndexByName(spinnerValue));
                }
                // go to next
                advanceCurrentPlayer();
                // if we hit the end, do computation, then set to day/game over
                if (pastLastPlayer()) {
                    nightOverComputation();
                    resetAll();
                    if (gameOver().equals(Constants.GAME_CONTINUE)) {
                        state = GameState.DAY;
                        unsuccessfulMotions = 0;
                    }
                    else state = GameState.OVER;
                }
                break;
            case DAY:
                if (getIndexByName(spinnerValue) == -1) {
                    state = GameState.NIGHT;
                }
                else {
                    vm = new VoteManager(players, spinnerValue);
                    state = GameState.VOTING;
                }
                break;
            case VOTING:
                if (!getCurrent().isDead() && !getCurrent().equals(vm.getAccused())) {
                    if (spinnerValue.equals("Yes"))
                        vm.vote(getCurrent().getVoteValue());
                }
                advanceCurrentPlayer();
                if (pastLastPlayer()) {
                    if (vm.voteSuccessful()) {
                        players[getIndexByName(vm.getAccused())].certainKill();
                        if (gameOver().equals(Constants.GAME_CONTINUE)) {
                            if (context != null) {
                                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
                                builder.setMessage("The vote passed. " + vm.getAccused() +
                                        " is dead.")
                                        .setTitle("Voting Results")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        });
                                android.support.v7.app.AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                            state = GameState.NIGHT;
                        }
                        else state = GameState.OVER;
                    }
                    else {
                        unsuccessfulMotions++;
                        // TODO: store accused, so that they can't be accused again
                        if (context != null) {
                            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
                            builder.setMessage("The vote did not pass. " + vm.getAccused() +
                                    " is still alive.")
                                    .setTitle("Voting Results")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });
                            android.support.v7.app.AlertDialog dialog = builder.create();
                            dialog.show();
                        }

                        if (unsuccessfulMotions >= 3)
                            state = GameState.NIGHT;
                        else state = GameState.DAY;
                    }
                }
                break;
            case OVER:
                break;
        }
    }
    // allows you to obtain index by name (assists in setting targets from
    // text input, easy conversion from String target to int index)
    public int getIndexByName(String name) {
        for (int i = 0; i < players.length; i++)
            if (players[i].getName().equals(name))
                return i;
        return -1;
    }

    // returns eligible target players.
    public ArrayList<String> getOptions() {
        ArrayList<String> returnList = new ArrayList<>();
        for (int i = 0; i < players.length; i++) {
            // don't add currentPlayer unless conditions apply
            if (i == currentPlayer) {
                if (!(players[currentPlayer]).canTargetSelf())
                    continue;
            }
            // don't add dead people
            if (players[i].isDead())
                continue;

            // if current player is Mafia, don't add other Mafia
            if (players[currentPlayer] instanceof Mafia) {
                if (players[i] instanceof Mafia)
                    continue;
            }
            // if current player is NOT Mafia, don't allow them to target
            // same person as the previous turn.
            else if (players[currentPlayer].getPreviousTarget() == i)
                continue;
            // if we made it this far, add them
            returnList.add(players[i].getName());
        }
        return returnList;
    }

    public Player get(int i) {
        if (i >= players.length)
            return null;
        return players[i];
    }

    public Player[] getAll() {
        return players;
    }

    public Player getCurrent() {
        return players[currentPlayer];
    }

    public int getNumMafia() {
        return numMafia;
    }

    public String getAccused() { return vm.getAccused(); }

    // info for game over/dead people.
    public String toString() {
        String retVal = "";
        for (Player player : players) {
            retVal += player.getName() + ": " + player.getRoleName();
            if (player.isDead())
                retVal += "(RIP)";
            retVal += "\n";
        }

        return retVal;
    }
}