package kl.android.mafiaapp;

/**
 * Created by kevin on 8/7/17.
 */

public enum GameState {
    DAY,
    NIGHT,
    SHOW_ROLE,
    VOTING,
    OVER
}
