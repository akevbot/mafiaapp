package kl.android.mafiaapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import kl.android.mafiaapp.player.Officer;
import kl.android.mafiaapp.player.Player;

import static android.view.View.GONE;

public class GameActivity extends AppCompatActivity {
    GameManager gm;
    TextView textView;
    Spinner spinner;
    Button submitButton;

    // TODO: move from Toasts to AlertDialogs in future.
    // TODO: add handling for final 3 (i.e. add spinner back)
    // TODO: add music playing for 2+ mafia at beginning of each night round
    // TODO: possibly add voting buttons rather than Yes/No dropdown

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // retrieve the names arraylist from prior activity
        Bundle bundle = this.getIntent().getBundleExtra("bundle");
        gm = (GameManager)bundle.getSerializable("gameManager");

        textView = (TextView)findViewById(R.id.target_text);
        // move to gm
        gm.setState(GameState.NIGHT);
        gm.setContext(this);
        spinner = (Spinner)findViewById(R.id.spinner);
        submitButton = (Button)findViewById(R.id.button_submit_target);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { buttonClicked(); } });
        // if more than 1 mafia, must do SHOW_ROLE, else straight to night
        if (gm.getNumMafia() > 1) gm.setState(GameState.SHOW_ROLE);
        else gm.setState(GameState.NIGHT);
        setUi();
    }

    public void buttonClicked() {
        // ensure that if a target must be chosen, it is
        if (gm.getCurrent().mustChooseTarget() && spinner.getSelectedItemPosition() == 0 &&
                gm.getState() == GameState.NIGHT) {
            Toast.makeText(this, "Your role must choose a target.", Toast.LENGTH_LONG).show();
            return;
        }

        // cop stuff
        if (gm.getCurrent() instanceof Officer) {
            gm.advanceGame((String)spinner.getSelectedItem());
            if (!Officer.report(gm).equals("Nothing to report yet.")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(Officer.report(gm))
                        .setTitle("Report")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }
        else gm.advanceGame((String)spinner.getSelectedItem());


        setUi();

    }

    // https://stackoverflow.com/questions/7383808/android-how-can-play-song-for-30-seconds-only-in-mediaplayer
    // for multiple mafia


    public void setUi() {
        ArrayList<String> stringArray;
        ArrayAdapter<String> adapter;
        switch (gm.getState()) {
            /*********************************** DAY ******************************************/
            case DAY:
                gm.setState(GameState.DAY);
                setTitle("Day");
                textView.setText("The game is still in progress. "+ gm.results +" Choose who you will execute during the day:");
                Player[] players = gm.getAll();

                spinner = (Spinner)findViewById(R.id.spinner);
                stringArray = new ArrayList<>();
                stringArray.add(0, "No one");
                for (Player player : players)
                    if (!player.isDead())
                        stringArray.add(player.getName());
                adapter = new ArrayAdapter<>(this,
                        R.layout.spinner_layout, stringArray);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
                break;
            /********************************** NIGHT *****************************************/
            case NIGHT:
                stringArray = gm.getOptions();
                stringArray.add(0, "No one");
                adapter = new ArrayAdapter<>(this,
                        R.layout.spinner_layout, stringArray);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
                spinner.setVisibility(View.VISIBLE);
                setTitle(gm.getCurrent().getName());
                textView.setText("Target:");
                break;
            /********************************** VOTING *****************************************/
            case VOTING:
                stringArray = new ArrayList<>();
                stringArray.add("Yes");
                stringArray.add("No");
                adapter = new ArrayAdapter<>(this,
                        R.layout.spinner_layout, stringArray);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
                spinner.setVisibility(View.VISIBLE);
                setTitle(gm.getCurrent().getName());
                textView.setText("Kill " + gm.getAccused() + "?");
                break;
            /*********************************** SHOW *****************************************/
            case SHOW_ROLE:
                spinner.setVisibility(GONE);
                submitButton.setText("OK");
                setTitle(gm.getCurrent().getName());
                ((TextView)findViewById(R.id.target_text)).setText("Click info button to see your role.");
                break;
            /*********************************** OVER *****************************************/
            case OVER:
                spinner.setVisibility(GONE);
                setTitle("Game Over");
                submitButton.setText("New Game");
                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startNewGame();
                    }
                });
                String gameOver = gm.gameOver();
                if (!gameOver.equals("You are down to the final three."))
                    gameOver += "\n" + gm.toString();
                textView.setText(gm.results + "\n" + gameOver);
                break;
        }
    }

    // don't need to touch any stuff below this
    public void startNewGame() {
        Intent i = new Intent(this, PlayerSetupActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_info) {
            // TODO: when mayor/other announceable roles are implemented, add the option to reveal them here

            if (gm.getState() == GameState.DAY) return super.onOptionsItemSelected(item);
            if (gm.getCurrent().isDead()) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("You are dead. Here is everyone's info: \n" + gm.toString())
                        .setTitle("RIP")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
            else Toast.makeText(this, "You are the " + gm.getCurrent().getRoleName() + ".",
                    Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // TODO: add alert dialog here
        //    finish();

    }
}
