package kl.android.mafiaapp;

/**
 * Created by kevin on 8/13/17.
 */

public class Constants {
    public static final String TOWN_WIN = "The Town has won!";
    public static final String MAFIA_WIN = "The Mafia have won!";
    public static final String FINAL_THREE = "You are down to the final three.";
    public static final String GAME_CONTINUE = "The game is still in progress.";
}
