package kl.android.mafiaapp.player;

import java.io.Serializable;

import kl.android.mafiaapp.GameManager;

/**
 * Created by Lukas on 5/25/2017.
 */

public abstract class Player implements Serializable {

    int primaryTarget, previousTarget;
    boolean isDead, isProtected;
    private String name;

    public Player() {
    }

    public abstract String getRoleName();

    public String toString() {
        return name;
    }

    public Player (String n) {
        name = n;
        isDead = false;
        isProtected = false;
        primaryTarget = -1;
        previousTarget = -1;
    }

    public void setPrimaryTarget(int n) {
        previousTarget = primaryTarget;
        primaryTarget = n;
    }

    public int getPrimaryTarget() {
        return primaryTarget;
    }

    public int getPreviousTarget() {
        return previousTarget;
    }

    public abstract boolean canTargetSelf();
    public abstract boolean mustChooseTarget();

    public boolean kill() {
        if (!isProtected) {
            isDead = true;
            return true;
        }
        return false;
    }
    
    //Kill that ignores protections, used by Veteran & Martyr, potentially more
	public void certainKill() {
		isDead = true;
		return;
	}

    public void protect() {
        isProtected = true;
    }

    public void reset() {
        if (!isDead) {
            isProtected = false;
            previousTarget = primaryTarget;
            primaryTarget = -1;
        }
    }

    // implemented on a per-role basis
    public abstract void setClassTarget(GameManager gm, int i);

    public abstract void resetClassTarget(GameManager gm);


    public String getName() {
        return name;
    }
    
    public int getVoteValue() { return 1; }

    public boolean isDead() { return isDead; }


	/* Rez method available for future implementations of roles
	public void rez() {
		//Could also just set isDead to False regardless
		if (isDead)
			isDead = false;
		else return
	}*/
}
