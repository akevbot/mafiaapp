package kl.android.mafiaapp.player;

import java.io.Serializable;

/**
 * Created by liferay on 6/20/17.
 */

public enum Role implements Serializable {
    OFFICER,
    MEDIC,
    VIGILANTE,
    VETERAN,
    ELDER,
    MARTYR,
    SURVIVALIST,
    MAYOR,
    EXECUTIONER
}
