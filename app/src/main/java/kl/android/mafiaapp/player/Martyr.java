package kl.android.mafiaapp.player;

import java.io.Serializable;

import kl.android.mafiaapp.GameManager;

/**
 * Created by Lukas on 6/29/2017.
 */

public class Martyr extends Player implements Serializable {
    public static int target = -1;
    public static boolean sacrifice = false;
    
	public String getRoleName() {
        return "Martyr";
    }

    public Martyr (String n) {
        super(n);
    }

    // implemented on a per-role basis
	//Very similar to Veteran, targeting self is only real option
    public void setClassTarget(GameManager gm, int i) {
        if (i < 0) return;
        if (gm.get(i).equals(this)) {
            Martyr.target = i;
			//no need for a count as the sacrifice can't be protected
            Martyr.sacrifice = true;
        }
    }

    public void resetClassTarget(GameManager gm) {
        Martyr.target = -1;
        Martyr.sacrifice = false;
    }

	//Disable self-targeting when dead just to be safe wrt sacrifice variable
    public boolean canTargetSelf() {
        return !(isDead);
    }

    public boolean mustChooseTarget() {
        return false;
    }
}