package kl.android.mafiaapp.player;

import java.io.Serializable;

import kl.android.mafiaapp.GameManager;

/**
 * Created by Kevin on 6/18/2017.
 */

public class Veteran extends Player implements Serializable {
    public static int target = -1;
    public static boolean onAlert = false;
    public static int usesLeft;
    public String getRoleName() {
        return "Veteran";
    }

    public Veteran (String n) {
        super(n);
        usesLeft = 1;
    }

    // implemented on a per-role basis
    public void setClassTarget(GameManager gm, int i) {
        if (i < 0) return;
        if (gm.get(i).equals(this)) {
            Veteran.target = i;
            Veteran.onAlert = true;
            this.protect();
        }
    }

    public void resetClassTarget(GameManager gm) {
        if (Veteran.onAlert) {
            Veteran.target = -1;
            Veteran.onAlert = false;
            Veteran.usesLeft--;
        }
    }

    public static boolean canGoOnAlert() {
        if (usesLeft > 0)
            return true;
        return false;
    }

    public boolean canTargetSelf() {
        return canGoOnAlert();
    }

    public boolean mustChooseTarget() {
        return false;
    }
}