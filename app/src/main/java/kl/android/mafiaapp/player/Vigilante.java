package kl.android.mafiaapp.player;

import java.io.Serializable;

import kl.android.mafiaapp.GameManager;

/**
 * Created by Kevin on 6/3/2017.
 */

public class Vigilante extends Player implements Serializable {
    public static boolean hasUsedShot = false;
    public static int target = -1;

    @Override
    public String getRoleName() {
        return "Vigilante";
    }

    public Vigilante(String n) {
        super(n);
        hasUsedShot = false;
        target = -1;
    }

    @Override
    public void setClassTarget(GameManager gm, int i) {
        if (i == -1) {
            return;
        }
        if (!hasUsedShot) {
            hasUsedShot = true;
            target = i;
        }
    }

    @Override
    public void resetClassTarget(GameManager gm) {
        target = -1;
    }

    public boolean canTargetSelf() {
        return false;
    }

    public boolean mustChooseTarget() {
        return false;
    }
}
