package kl.android.mafiaapp.player;

import java.io.Serializable;

import kl.android.mafiaapp.GameManager;

/**
 * Created by Kevin on 6/3/2017.
 */

public class Officer extends Player implements Serializable {
    public static int pastTarget = -1, currentTarget = -1;

    @Override
    public String getRoleName() {
        return "Officer";
    }

    public Officer(String n) {
        super(n);
        pastTarget = -1;
        currentTarget = -1;
    }

    @Override
    public void setClassTarget(GameManager gm, int i) {
        pastTarget = currentTarget;
        currentTarget = i;
    }

    @Override
    public void resetClassTarget(GameManager gm) {
        return;
    }

    public static String report(GameManager gm) {
        String retVal = "";
        if (pastTarget == -1)
            return "Nothing to report yet.";
        else {
            Player p1 = gm.get(currentTarget);
            Player p2 = gm.get(pastTarget);

            if (p1 instanceof Mafia ^ p2 instanceof Mafia)
                return p1.getName() + " and " + p2.getName() + " ARE NOT on the same team.";
            else return p1.getName() + " and " + p2.getName() + " ARE on the same team.";
        }
    }

    public boolean canTargetSelf() {
        return false;
    }

    public boolean mustChooseTarget() {
        return true;
    }
}
