package kl.android.mafiaapp.player;

import java.io.Serializable;

import kl.android.mafiaapp.GameManager;

/**
 * Created by kevin on 7/2/17.
 */

public class Survivalist extends Player implements Serializable {
    boolean isWearingVest, hasUsedVest;
    int numberOfBullets;

    // bulletsTankable can be initialized using Settings variable
    public Survivalist(String name, int bulletsTankable) {
        super(name);
        isWearingVest = true;
        hasUsedVest = true;
        numberOfBullets = bulletsTankable;
        //if we decide to just have it on all the time, make
        //hasUsedVest and isWearingVest true here and remove setClassTarget logic
    }
    @Override
    public String getRoleName() {
        String warning ="";
        if (hasUsedVest && numberOfBullets <= 0)
            warning = ".  Your bulletproof vest has been destroyed";
        return "Survivalist" + warning;
    }

    @Override
    public boolean canTargetSelf() {
        return !hasUsedVest;
    }

    @Override
    public void setPrimaryTarget(int n) {
        return;
    }


    @Override
    public boolean mustChooseTarget() {
        return false;
    }

    @Override
    public boolean kill() {
        if (isProtected)
            return false;
        if (!isWearingVest) {
            isDead = true;
            return true;
        }
        else {
            numberOfBullets--;
            if (numberOfBullets == 0)
                isWearingVest = false;
            return false;
        }

    }

    @Override
    public void setClassTarget(GameManager gm, int i) {
        /*if (gm.get(i).equals(this)) {
            hasUsedVest = true;
            isWearingVest = true;
        }*/
    }

    @Override
    public void resetClassTarget(GameManager gm) {
        return;
    }
}
