package kl.android.mafiaapp.player;

import java.io.Serializable;

import kl.android.mafiaapp.GameManager;

/**
 * Created by Kevin on 6/2/2017.
 */

public class Medic extends Player implements Serializable {

    @Override
    public String getRoleName() {
        return "Medic";
    }

    public Medic(String n)
    {
        super(n);
    }

    public void setClassTarget(GameManager gm, int i) {
        gm.get(i).protect();
    }

    public void resetClassTarget(GameManager gm) {
        return;
    }

    public boolean canTargetSelf() {
        return true;
    }

    public boolean mustChooseTarget() {
        return true;
    }
}