package kl.android.mafiaapp.player;

import java.io.Serializable;

import kl.android.mafiaapp.GameManager;

/**
 * Created by Kevin on 6/2/2017.
 */

public class Elder extends Player implements Serializable {

    @Override
    public String getRoleName() {
        return "Elder";
    }

    public Elder(String n)
    {
        super(n);
        // protect on the first night (is removed after the 1st night using reset())
        this.isProtected = true;
    }

    @Override
    public void setPrimaryTarget(int n) {
        return;
    }
    @Override
    public void setClassTarget(GameManager gm, int i) {
        return;
    }

    @Override
    public void resetClassTarget(GameManager gm) {
        return;
    }

    public boolean canTargetSelf() {
        return false;
    }

    public boolean mustChooseTarget() {
        return false;
    }
}