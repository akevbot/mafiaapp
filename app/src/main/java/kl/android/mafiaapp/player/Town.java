package kl.android.mafiaapp.player;

import java.io.Serializable;
import kl.android.mafiaapp.GameManager;

/**
 * Created by Kevin on 6/3/2017.
 */

public class Town extends Player implements Serializable {

    @Override
    public String getRoleName() {
        return "Townsperson";
    }

    public Town(String n)
    {
        super(n);
    }

    @Override
    public void setPrimaryTarget(int n) {
        return;
    }

    @Override
    public void setClassTarget(GameManager gm, int i) {
        return;
    }

    @Override
    public void resetClassTarget(GameManager gm) {
        return;
    }

    public boolean canTargetSelf() {
        return false;
    }

    public boolean mustChooseTarget() {
        return false;
    }
}
