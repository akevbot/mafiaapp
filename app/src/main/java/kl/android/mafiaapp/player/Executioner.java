package kl.android.mafiaapp.player;

import java.io.Serializable;

import kl.android.mafiaapp.GameManager;

public class Executioner extends Player implements Serializable {

    @Override
    public String getRoleName() {
        return "Executioner";
    }

    public Executioner(String n) {
        super(n);
    }

    @Override
    public void setPrimaryTarget(int n) {
        return;
    }
    @Override
    public void resetClassTarget(GameManager gm) {
        return;
    }

    public boolean canTargetSelf() {
        return false;
    }

    public boolean mustChooseTarget() {
        return false;
    }

    @Override
    public void setClassTarget(GameManager gm, int i) {

    }

    public int getVoteValue() { return 400; }
}