package kl.android.mafiaapp.player;

import java.io.Serializable;

import kl.android.mafiaapp.GameManager;

/**
 * Created by Kevin on 6/2/2017.
 */
public class Mafia extends Player implements Serializable {
    public static int mafiaTarget = -1;
    public static boolean mafiaError = false;

    @Override
    public String getRoleName() {
        return "Mafia";
    }

    public Mafia(String n)
    {
        super(n);
    }

    public void setClassTarget(GameManager gm, int i) {
        if (mafiaTarget == -1)
            mafiaTarget = i;
        else if (mafiaTarget != i) {
            Mafia.setFlag();
            return;
        }
    }

    public void resetClassTarget(GameManager gm) {
        mafiaTarget = -1;
        mafiaError = false;
    }

    public boolean canTargetSelf() {
        return false;
    }

    public boolean mustChooseTarget() {
        return true;
    }


    public static void setFlag() {
        mafiaError = true;
    }
}