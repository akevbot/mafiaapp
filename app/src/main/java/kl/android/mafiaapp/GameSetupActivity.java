package kl.android.mafiaapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import org.apache.commons.lang3.text.WordUtils;

import java.util.ArrayList;
import java.util.HashMap;

import kl.android.mafiaapp.player.Role;

public class GameSetupActivity extends AppCompatActivity {

    ArrayList<String> names;
    ListView lv;
    ArrayList<String> roleNames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_setup);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        // retrieve role names, wrap them in CheckBoxes
        roleNames = new ArrayList<>(0);
        for (Role role : Role.values()) {
            roleNames.add(WordUtils.capitalizeFully(role.toString()));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_multiple_choice, roleNames);
        lv = (ListView)findViewById(R.id.checkbox_listview);
        lv.setAdapter(adapter);
        lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        // get persisted value, check the proper items.
        SharedPreferences sharedPreferences = this.getSharedPreferences(
                "kl.android.mafiaapp.GAME_SETTINGS_FILE", Context.MODE_PRIVATE);
        String setBooleans = sharedPreferences.getString("settings", "");
        String[] settings = setBooleans.split("/");
        ((EditText)findViewById(R.id.number_mafia)).setText(settings[0]);
        for (int i = 1; i < settings.length; i++)
            lv.setItemChecked(i - 1, settings[i].equals("t"));

        // retrieve the names arraylist from prior activity
        Bundle bundle = this.getIntent().getBundleExtra("bundle");
        names = (ArrayList<String>)bundle.getSerializable("namesList");
        Button doneButton = (Button)findViewById(R.id.button_submit_game_options);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGameActivity();
            }
        });

    }

    // starts new game activity, passes game manager w/ options and players already created.
    public void startGameActivity()
    {
        EditText numMafia = (EditText)findViewById(R.id.number_mafia);

        HashMap<Role, Boolean> isEnabled = new HashMap<>();
        SparseBooleanArray checked = lv.getCheckedItemPositions();

        // put role enum for key, if checked for value
        String prefs = "" + Integer.valueOf(numMafia.getText().toString());
        int k = 0;
        for (Role role : Role.values()) {
            isEnabled.put(role, checked.get(k));
            if (checked.get(k)) prefs += "/t";
            else prefs += "/f";
            k++;
        }
        SharedPreferences sharedPreferences = this.getSharedPreferences(
                "kl.android.mafiaapp.GAME_SETTINGS_FILE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("settings", prefs);
        editor.commit();

        GameManager gm = new GameManager(names.toArray(new String[names.size()]),
                Integer.parseInt(numMafia.getText().toString()),
                isEnabled, this);

        Bundle bundle = new Bundle();

        bundle.putSerializable("gameManager", gm);
        Intent i = new Intent(this, GameActivity.class);
        i.putExtra("bundle", bundle);
        startActivity(i);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game_setup, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, PlayerSetupActivity.class));
        finish();
        super.onBackPressed();
    }

}
