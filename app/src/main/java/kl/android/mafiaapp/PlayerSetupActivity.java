package kl.android.mafiaapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.woxthebox.draglistview.DragListView;

import java.util.ArrayList;

public class PlayerSetupActivity extends AppCompatActivity {

    DragListView mDragListView;
    ArrayList mItemArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_setup);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // drag list view stuff
        mDragListView = (DragListView)findViewById(R.id.drag_list_view);
        mDragListView.getRecyclerView().setVerticalScrollBarEnabled(true);

        SharedPreferences sharedPreferences = this.getSharedPreferences(
                "kl.android.mafiaapp.NAMES_FILE", Context.MODE_PRIVATE);
        String existingNames = sharedPreferences.getString("names", "");
        String[] names = existingNames.split("/");
        mItemArray = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            mItemArray.add(new Pair<>((long) i, names[i]));
        }

        mDragListView.setLayoutManager(new LinearLayoutManager(this));
        ItemAdapter listAdapter = new ItemAdapter(mItemArray, R.layout.list_item, R.id.image, false);
        mDragListView.setAdapter(listAdapter, true);
        mDragListView.setCanDragHorizontally(false);

        final Button mAddButton = (Button)findViewById(R.id.button_add);
        Button mDoneButton = (Button)findViewById(R.id.button_role_setup);

        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAlertDialog();
            }
        });
        mDoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPlayerSetupActivity();
            }
        });
    }

    public void createAlertDialog()
    {
        // use builder to create the dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // declare as final to allow inner class to access it
        final EditText input = new EditText(this);
        input.setPadding(80,40,80,40);

        // setView allows you to add custom views
        builder.setView(input);
        //set OK button
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String name = input.getText().toString();
                mItemArray.add(new Pair<>((long)mItemArray.size(), name));
                dialog.dismiss();
            }
        });
        // Cancel button
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                dialog.dismiss();
            }
        });
        builder.setTitle("Add New Player");
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void startPlayerSetupActivity()
    {
        Bundle bundle = new Bundle();

        ArrayList<String> mNamesArray = new ArrayList<String>();
        String prefs = "";
        for (int i = 0; i < mItemArray.size(); i++)
        {
            // this looks messy as hell; essentially we retrieve each
            // Pair object, cast it to pair, then retrieve each second object
            // (the person's name) and cast it to a string.
            // then add it to the names array.
            mNamesArray.add((String)(((Pair)mItemArray.get(i)).second));
            if (i != 0)
                prefs += "/" + mNamesArray.get(i);
            else prefs += mNamesArray.get(i);
        }
        SharedPreferences sharedPreferences = this.getSharedPreferences(
                "kl.android.mafiaapp.NAMES_FILE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("names", prefs);
        editor.commit();
        bundle.putSerializable("namesList", mNamesArray);
        Intent i = new Intent(this, GameSetupActivity.class);
        i.putExtra("bundle", bundle);
        startActivity(i);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game_setup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
