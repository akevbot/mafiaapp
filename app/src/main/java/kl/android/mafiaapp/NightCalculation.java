package kl.android.mafiaapp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import kl.android.mafiaapp.player.Mafia;
import kl.android.mafiaapp.player.Martyr;
import kl.android.mafiaapp.player.Player;
import kl.android.mafiaapp.player.Veteran;
import kl.android.mafiaapp.player.Vigilante;

/**
 * Created by kevin on 8/7/17.
 */

public class NightCalculation {

    Player[] players;

    public NightCalculation(Player[] players)
    {
        this.players = players;
    }

    public String nightOverComputation() {
        // Set will prevent duplicates.
        Set<String> deadNameSet = new HashSet<>();
        ArrayList<Player> mafia = new ArrayList<>(0);

        /*
		* Martyr sacrifice nullifies all other kill actions,
		* therefore we return after the sacrifice since all
		* additional handling here is for killing.
		*/

        //Martyr first
        if (Martyr.sacrifice)
            for (Player player : players)
                if (player instanceof Martyr) {
                    player.certainKill();
                    return player.getName() + " died last night.";
                }

        // Veteran second
        if (Veteran.onAlert) {
            int i;
            // find who the veteran is
            for (i = 0; i < players.length; i++) {
                if (players[i] instanceof Veteran)
                    break;
            }
            // find who is targeting the veteran
            for (Player player : players) {
                if (player.getPrimaryTarget() == i && !(player instanceof Veteran)) {
                    // if they're mafia add them to a list
                    if (player instanceof Mafia && !player.isDead())
                        mafia.add(player);
                        // else kill them
                    else {
                        player.certainKill();
                        deadNameSet.add(player.getName());
                    }
                }
            }

            // choose one of the mafia to kill
            Random rng = new Random();
            int mafiaToKill = rng.nextInt(mafia.size());
            mafia.get(mafiaToKill).certainKill();
            deadNameSet.add(mafia.get(mafiaToKill).getName());
        }
        if (players[Mafia.mafiaTarget].kill())
            deadNameSet.add(players[Mafia.mafiaTarget].getName());
        if (Vigilante.target != -1 && players[Vigilante.target].kill())
            deadNameSet.add(players[Vigilante.target].getName());
        if (deadNameSet.isEmpty())
            return "No one died last night.";

        String retVal = "";

        String[] deadNamesArray = new String[deadNameSet.size()];
        // moves the Set objects to an array
        deadNameSet.toArray(deadNamesArray);
        // add the names to the return String
        for (int i = 0; i < deadNamesArray.length; i++) {
            if (i == 0)
                retVal += deadNamesArray[0];
            else if (i == deadNamesArray.length - 1)
                retVal += " and " + deadNamesArray[i];
            else retVal += ", " + deadNamesArray[i];
        }
        retVal += " died last night.";
        return retVal;
    }
}
